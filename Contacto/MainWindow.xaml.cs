﻿
using System.Windows.Controls;
using System.Windows.Data;
using Contacto.Model;
using Contacto.Presenters;
using NLog;

namespace Contacto
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        // Logger!
        // https://github.com/nlog/nlog/wiki/Tutorial
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ApplicationPresenter(this, new ContactRepositoryXML());

            Logger.Info("MainWindow constructor");

        }
    

        public void AddTab<T>(PresenterBase<T> presenter)
        {
            TabItem newTab = null;
            for (var i = 0; i < tabs.Items.Count; i++)
            {
                var existingTab = (TabItem)tabs.Items[i];
                if (existingTab.DataContext.Equals(presenter))
                {
                    tabs.Items.Remove(existingTab);
                    newTab = existingTab;
                    break;
                }
            }
            if (newTab == null)
            {
                newTab = new TabItem();
                var headerBinding = new Binding(presenter.TabHeaderPath);
                BindingOperations.SetBinding(
                newTab,
                HeaderedContentControl.HeaderProperty,
                headerBinding
                );
                newTab.DataContext = presenter;
                newTab.Content = presenter.View;
            }
            tabs.Items.Insert(0, newTab);
            newTab.Focus();
            Logger.Info("Add Tab");
        }

        public void RemoveTab<T>(PresenterBase<T> presenter)
        {
            for (var i = 0; i < tabs.Items.Count; i++)
            {
                var item = (TabItem)tabs.Items[i];
                if (item.DataContext.Equals(presenter))
                {
                    tabs.Items.Remove(item);
                    break;
                }
            }
        }
    }
}
