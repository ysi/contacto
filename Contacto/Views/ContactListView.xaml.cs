﻿using System.Windows;
using System.Windows.Controls;
using Contacto.Model;
using Contacto.Presenters;

namespace Contacto.Views
{
    /// <summary>
    /// Interaction logic for ContactListView.xaml
    /// </summary>
    public partial class ContactListView : UserControl
    {
        public ContactListView()
        {
            InitializeComponent();
        }

        public ContactListPresenter Presenter => DataContext as ContactListPresenter;

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Presenter.Close();
        }

        private void OpenContact_Click(object sender, RoutedEventArgs e)
        {
            // edit selected contact
            var index = contactListView.SelectedIndex;
            if (index < 0) return;
            var contact = (Contact)contactListView.SelectedItem;
            Presenter.OpenContact(contact);
        }
    }
}
