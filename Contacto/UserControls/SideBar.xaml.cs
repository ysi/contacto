﻿using System.Windows;
using System.Windows.Controls;
using Contacto.Model;
using Contacto.Presenters;
using NLog;

namespace Contacto.UserControls
{
    /// <summary>
    /// Interaction logic for SideBar.xaml
    /// </summary>
    public partial class SideBar : UserControl
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public SideBar()
        {
            InitializeComponent();
        }

        private ApplicationPresenter Presenter => DataContext as ApplicationPresenter;

        private void New_Click(object sender, RoutedEventArgs e)
        {
            Presenter.NewContact();
        }

        private void ViewAll_Click(object sender, RoutedEventArgs e)
        {
            Presenter.DisplayAllContacts();
        }

        private void OpenContact_Click(object sender, RoutedEventArgs e)
        {
            // edit selected contact
            var index = ContactList.SelectedIndex;
            if (index < 0) return;
            var contact = (Contact) ContactList.SelectedItem;
            Presenter.OpenContact(contact);
        }

    }
}
