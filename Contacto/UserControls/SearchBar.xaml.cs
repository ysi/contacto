﻿using System.Windows.Controls;
using Contacto.Presenters;

namespace Contacto.UserControls
{
    /// <summary>
    /// Interaction logic for SearchBar.xaml
    /// </summary>
    public partial class SearchBar : UserControl
    {
        public SearchBar()
        {
            InitializeComponent();
        }

        public ApplicationPresenter Presenter => DataContext as ApplicationPresenter;

        private void SearchText_Changed(object sender, TextChangedEventArgs e)
        {
            Presenter.Search(searchText.Text);
        }
    }
}
