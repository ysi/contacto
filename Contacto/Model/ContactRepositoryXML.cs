﻿using System;
using System.Data;
using System.IO;
using NLog;

namespace Contacto.Model
{
    public class ContactRepositoryXML : ContactRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        private readonly string _stateFile;


        public ContactRepositoryXML()
        {
            // TODO put filename to resources
            _stateFile = Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory,
                "ContactoDB.xml"
                );

            // TODO should load logic be here?
            LoadData();
        }

        protected override void LoadDataSet()
        {
            try
            {
                dataSet.ReadXml(_stateFile, XmlReadMode.ReadSchema);
            }
            catch (Exception e)
            {
                Logger.Error("Error when reading XML file");
                
                // Save empty dataset
                SaveDataSet();
            }
            
        }


        protected override void SaveDataSet()
        {
            dataSet.WriteXml(_stateFile, XmlWriteMode.WriteSchema);
        }
    }
}