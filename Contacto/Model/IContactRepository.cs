﻿using System.Collections.Generic;

namespace Contacto.Model
{
    public interface IContactRepository
    {
        void Save(Contact contact);
        void Delete(Contact contact);
        List<Contact> FindByLookup(string lookupName);
        List<Contact> FindAll();
    }
}
