﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Contacto.Model
{

    public abstract class ContactRepository : IContactRepository
    {
        private List<Contact> _contactStore;
        protected readonly ContactoDataSet dataSet = new ContactoDataSet();

        private static void MoveContactInfo(Contact contact, ContactoDataSet.ContactsRow contactRow)
        {
            contactRow.FirstName = contact.FirstName;
            contactRow.LastName = contact.LastName;
            contactRow.ContactID = contact.Id;
            contactRow.Organization = contact.Organization;
            contactRow.JobTitle = contact.JobTitle;
            contactRow.PrimaryEmail = contact.PrimaryEmail;
            contactRow.SecondaryEmail = contact.SecondaryEmail;
            contactRow.OfficePhone = contact.OfficePhone;
            contactRow.HomePhone = contact.HomePhone;
            contactRow.CellPhone = contact.CellPhone;
            contactRow.Country = contact.Address.Country;
            contactRow.State = contact.Address.State;
            contactRow.Zip = contact.Address.Zip;
            contactRow.City = contact.Address.City;
            contactRow.Line1 = contact.Address.Line1;
            contactRow.Line2 = contact.Address.Line2;
            contactRow.HomeCountry = contact.HomeAddress.Country;
            contactRow.HomeState = contact.HomeAddress.State;
            contactRow.HomeZip = contact.HomeAddress.Zip;
            contactRow.HomeCity = contact.HomeAddress.City;
            contactRow.HomeLine1 = contact.HomeAddress.Line1;
            contactRow.HomeLine2 = contact.HomeAddress.Line2;
            contactRow.ImagePath = contact.ImagePath;
            contactRow.Comments = contact.Comments;
            contactRow.BirthDate = contact.Birthdate;
        }

        private static void MoveContactInfo(ContactoDataSet.ContactsRow contactRow, Contact contact)
        {
            contact.FirstName = contactRow.FirstName;
            contact.LastName = contactRow.LastName;
            contact.Id = contactRow.ContactID;
            contact.Organization = contactRow.Organization;
            contact.JobTitle = contactRow.JobTitle;
            contact.OfficePhone = contactRow.OfficePhone;
            contact.HomePhone = contactRow.HomePhone;
            contact.CellPhone = contactRow.CellPhone;
            contact.PrimaryEmail = contactRow.PrimaryEmail;
            contact.SecondaryEmail = contactRow.SecondaryEmail;
            contact.ImagePath = contactRow.ImagePath;
            contact.Birthdate = contactRow.BirthDate;
            contact.Address.Country = contactRow.Country;
            contact.Address.State = contactRow.State;
            contact.Address.Zip = contactRow.Zip;
            contact.Address.City = contactRow.City;
            contact.Address.Line1 = contactRow.Line1;
            contact.Address.Line2 = contactRow.Line2;
            contact.HomeAddress.Country = contactRow.HomeCountry;
            contact.HomeAddress.State = contactRow.HomeState;
            contact.HomeAddress.Zip = contactRow.HomeZip;
            contact.HomeAddress.City = contactRow.HomeCity;
            contact.HomeAddress.Line1 = contactRow.HomeLine1;
            contact.HomeAddress.Line2 = contactRow.HomeLine2;
            contact.Comments = contactRow.Comments;
        }

        private void MoveContactInfoToStore()
        {
            // TODO how to access the Contacts table without index?
            _contactStore = new List<Contact>();
            foreach (var cr in dataSet.Contacts.Rows)
            {
                var contactRow = (ContactoDataSet.ContactsRow)cr;
                var contact = new Contact();
                MoveContactInfo(contactRow, contact);
                _contactStore.Add(contact);
            }

        }

        public void Save(Contact contact)
        {
            // correct wrong ID
            if (contact.Id < 1)
            {
                contact.Id = 1 + _contactStore.Select(c => c.Id).Concat(new[] { 0 }).Max();
            }

            if (_contactStore.Contains(contact))
            {
                // edit DataSet row in situ
                var contactsRow = dataSet.Contacts.FindByContactID(contact.Id);
                MoveContactInfo(contact, contactsRow);
            }
            else
            {
                // add contact to List
                _contactStore.Add(contact);

                // add contact to DataSet
                var contactsRow = dataSet.Contacts.NewContactsRow();
                MoveContactInfo(contact, contactsRow);
                dataSet.Contacts.Rows.Add(contactsRow);
            }

            SaveDataSet();
        }

        public void Delete(Contact contact)
        {
            _contactStore.Remove(contact);
            dataSet.Contacts.Rows.Find(contact.Id).Delete();
            SaveDataSet();
        }

        public List<Contact> FindByLookup(string criteria) => _contactStore.Where(c => c.GetInfoString().IndexOf(criteria, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

        public List<Contact> FindAll() => new List<Contact>(_contactStore);

        protected abstract void LoadDataSet();

        protected abstract void SaveDataSet();

        protected void LoadData()
        {
            LoadDataSet();
            MoveContactInfoToStore();
        }


    }
}
