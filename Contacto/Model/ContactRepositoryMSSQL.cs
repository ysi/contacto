﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contacto.ContactoDataSetTableAdapters;
using NLog;

namespace Contacto.Model
{
    public class ContactRepositoryMSSQL : ContactRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        // TODO hashed password should be stored in config file
        private const string Password = ";Password=Schubert";

        private readonly ContactsTableAdapter _cta;

        public ContactRepositoryMSSQL()
        {
            Logger.Info("ContactRepositoryMSSQL constructor");

            _cta = new ContactsTableAdapter();
            _cta.Connection.ConnectionString += Password;

            // TODO should load logic be here?
            LoadData();

            Logger.Info("Contacts Count=" + dataSet.Contacts.Rows.Count);
        }

        protected override void LoadDataSet()
        {
            try
            {
                _cta.Fill(dataSet.Contacts);
            }
            catch (Exception e)
            {
                Logger.Error("Error when reading from database");

                // Save empty dataset
                Logger.Error("Let's try to save empty dataset");
                SaveDataSet();
            }
        }


        protected override void SaveDataSet()
        {

            try
            {
                _cta.Update(dataSet.Contacts);
                Logger.Info("Update DB finished");
            }
            catch (Exception e)
            {
                Logger.Error("Error when writing to database");
                Logger.Error("DB is unaccessible or has wrong schema");
                // TODO we may try to create database schema here
            }
            
            
        }
    }
}
