﻿using System.Collections.ObjectModel;
using Contacto.Model;
using Contacto.Views;
using NLog;

namespace Contacto.Presenters
{
    public class ApplicationPresenter : PresenterBase<MainWindow>
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IContactRepository _contactRepository;
        private ObservableCollection<Contact> _currentContacts;
        private string _statusText;

        public ApplicationPresenter(
            MainWindow view,
            IContactRepository contactRepository)
            : base(view)
        {
            _contactRepository = contactRepository;

            _currentContacts = new ObservableCollection<Contact>(
                _contactRepository.FindAll()
                );
        }

        public ObservableCollection<Contact> CurrentContacts
        {
            get { return _currentContacts; }
            set
            {
                _currentContacts = value;
                OnPropertyChanged("CurrentContacts");
            }
        }

        public string StatusText
        {
            get { return _statusText; }
            set
            {
                _statusText = value;
                OnPropertyChanged("StatusText");
            }
        }

        public void Search(string criteria)
        {
            if (!string.IsNullOrEmpty(criteria) && criteria.Length > 1)
            {
                CurrentContacts = new ObservableCollection<Contact>(
                    _contactRepository.FindByLookup(criteria)
                    );

                StatusText = $"{CurrentContacts.Count} contacts found.";
            }
            else
            {
                CurrentContacts = new ObservableCollection<Contact>(
                    _contactRepository.FindAll()
                    );

                StatusText = "Displaying all contacts.";
            }
        }

        public void NewContact()
        {
            OpenContact(new Contact());
        }

        public void SaveContact(Contact contact)
        {
            // TODO there is a need to rethink the update logic
            // when we save modified or new contact
            // the CurrentContacts list should be updated
            // by performing a search with the current criteria

            // how to obtain the current criteria here?
            // SearchBar.xaml - TextBox - x:Name="searchText"
            Logger.Info("Current search criteria: "        );


            if (CurrentContacts.Contains(contact))
                CurrentContacts.Remove(contact);
            CurrentContacts.Add(contact);

            _contactRepository.Save(contact);

            StatusText = $"Contact '{contact.LookupName}' was saved.";
        }

        public void DeleteContact(Contact contact)
        {
            if (CurrentContacts.Contains(contact))
                CurrentContacts.Remove(contact);

            _contactRepository.Delete(contact);

            StatusText = $"Contact '{contact.LookupName}' was deleted.";
        }

        public void CloseTab<T>(PresenterBase<T> presenter)
        {
            View.RemoveTab(presenter);
        }

        public void OpenContact(Contact contact)
        {
            if (contact == null) return;
            View.AddTab(
            new EditContactPresenter(
            this,
            new EditContactView(),
            contact
            )
            );
        }

        public void DisplayAllContacts()
        {
            View.AddTab(
                new ContactListPresenter(
                    this,
                    new ContactListView()
                    )
                );
        }

    }


}
