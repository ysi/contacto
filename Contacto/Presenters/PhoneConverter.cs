﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Contacto.Presenters
{
    public class PhoneConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = value as string;

            if (!string.IsNullOrEmpty(result))
            {
                var filteredResult = FilterNonNumeric(result);

                var theNumber = System.Convert.ToInt64(filteredResult);

                switch (filteredResult.Length)
                {
                    case 12:
                        result = $"{theNumber:+### (##) ###-##-##}";
                        break;
                    case 9:
                        result = $"{theNumber:(##) ###-##-##}";
                        break;
                    case 7:
                        result = $"{theNumber:###-##-##}";
                        break;
                }
            }
            return result;
        }

        private static string FilterNonNumeric(string stringToFilter)
        {
            if (string.IsNullOrEmpty(stringToFilter)) return string.Empty;

            var filterQuery = stringToFilter.Where(char.IsDigit);
            return new string(filterQuery.ToArray());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return FilterNonNumeric(value as string);
        }
    }
}
