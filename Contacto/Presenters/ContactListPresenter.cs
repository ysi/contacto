﻿using System.Collections.ObjectModel;
using Contacto.Model;
using Contacto.Views;

namespace Contacto.Presenters
{
    public class ContactListPresenter : PresenterBase<ContactListView>
    {
        private readonly ApplicationPresenter _applicationPresenter;

        public ContactListPresenter(
            ApplicationPresenter applicationPresenter,
            ContactListView view)
            : base(view, "TabHeader")
        {
            _applicationPresenter = applicationPresenter;
        }

        public string TabHeader => "All Contacts";

        public void Close()
        {
            _applicationPresenter.CloseTab(this);
        }

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType();
        }

        // exposing the collection so that it is
        // accessible in the view
        public ObservableCollection<Contact> AllContacts => _applicationPresenter.CurrentContacts;

        public void OpenContact(Contact contact)
        {
            _applicationPresenter.OpenContact(contact);
        }
    }
}
